const path = require('path');
const ASSET_PATH = process.env.ASSET_PATH || '/';

module.exports = {
  entry: './src/index.ts',
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: ['ts-loader'],
        exclude: /node_modules/,
      }
    ]
  },
  resolve: {
    extensions: ['.tsx', '.ts'],
  },
  output: {
    filename: 'bubble-chart.js',
    path: path.resolve(__dirname, 'webroot'),
    publicPath: ASSET_PATH
  }
}