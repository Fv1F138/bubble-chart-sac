![](images/example.jpg)
# SAC Bubble Chart Diagram (Praktikatenaufgabe)

Lange Zeit wurden Fluorchlorkohlenwasserstoffe als Treibgas oder Kältemittel in der Industrie eingesetzt ohne das man sich über die Folgen für die Umwelt bewusst war. Erst spät hat man den negativen Effekt der Gase auf die Ozon Schicht bemerkt und die Benutzung selbiger in der Industrie verboten. Seit dem haben Länder die von den Ozon Löchern betroffen sind mit immer mehr Problemen durch beispielsweise Hautkrebs zu kämpfen.

## Aufgabe
Visualisiere die aktuelle Verteilung von Ozon oder CO2 in der Atmosphäre innerhalb der  [SAP Cloud Analytics Platform](https://five1.eu10.hcs.cloud.sap/) mit Hilfe eines [Bubble Chart Diagrammes](https://de.wikipedia.org/wiki/Blasendiagramm). Nutze hierbei die öffentlich zugängliche [emissions-api](https://emissions-api.org/) um die notwendigen Daten für die Visualisierung zu erlangen. <br>
Das Projekt ist für die Programmierung mit [Typescript](https://www.typescriptlang.org/) aufgesetzt. Gerne kann dies aber auch durch Javascript ersetzt werden.

## Development

```bash
git clone https://gitlab.com/Fv1F138/bubble-chart-sac
cd bubble-chart-sac

# install development deps like webpack, etc.
npm install

# start webpack build process
npm run-script build

#start development server
npm run-script server
```


> **Info:** VS Code users could run build and server script by clicking corresponding button below NPM scripts.

## Installation
> Bitte in [bubble-chart.json](src/bubble-chart.json) *MYNAME* durch eigenen Namen ersetzen!

Füge [src/bubble-chart.json](src/bubble-chart.json) in der SAC unter Benutzerdefinierte Widgets (Durchsuchen > Benutzerdefinierte Widgets) hinzu.

## Benötigte Software für das Praktikum
- VS Code
- NodeJS (LTS)
- Git Bash (https://gitforwindows.org/)
- Google Chrome
- LibreOffice
- Clouduser für die SAC

## Nützliche Links
- https://five1.eu10.hcs.cloud.sap/
- https://emissions-api.org/
- https://blogs.sap.com/2019/12/06/build-a-custom-widget-in-sap-analytics-cloud-analytics-application/